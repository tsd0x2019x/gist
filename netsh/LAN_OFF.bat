@REM
@REM netsh.bat
@REM
@REM Windows command to manually (de)activate LAN connection.
@REM
@REM Type "netsh ?" to display commands for netsh
@REM Type "netsh interface ?" to display commands for "net interface"
@REM Type "netsh interface set interface ?" to display more commands and examples
@REM
@REM
@REM Note: Run 'netsh interface' as Administrator.
@REM

netsh interface set interface name="LAN-Verbindung" admin=DISABLED