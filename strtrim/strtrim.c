
/**
 * strtrim.c
 * 
 * Demonstrate an implementation of a typical trimming function 
 * for eliminating the leading and ending spaces within a string.
 * 
 */
#include <stdio.h>
#include <string.h>
#define MAX 101 // including the null-terminating charater '\0'

/**
 * strtrim
 * 
 * Trim/Eliminate all leading and ending space characters (32)
 * within a string. This function modifies the input string.
 * 
 * @param
 *      input : string that will be trimmed
 * 
 * @return
 *      Pointer to the first character of the trimmed (result) string
 **/
char * strtrim (char * input) {
    int i = 0;
    // Attempt to remove all leading space characters.
    //
    while (input[i]) {
        if (input[i] != 32) // 32 is Ascii code for Line-feed / Page-break / Carriage-return
            break;
        ++i;
    }
    // Attempt to shift all non-space characters to left side
    //
    if (i > 0) { // index i of the first character that is not space
        int d = i;
        while (input[i]) {
            input[i-d] = input[i];
            ++i;
        }
    }
    // Attempt to remove all ending space characters.
    //
    i = (int)strlen(input) - 1;
    while ((input[i] == 0 || input[i] == 32) && i >= 0) { // Attempt to bypass all ending space characters
        input[i] = 0;
        --i;
    }
    //
    return input;
}

/**
 * Print_String
 */
void Print_String (char * input) {
    printf("\n");
    char * c = input;
    while (*c) {
        if (*c == 32) {
            printf("_"); // underscore '_' illustrates the space character
        }
        else {
            printf("%c", *c);
        }
        ++c;
    }
}

/**
 * Main function
 */
int main(int argc, char * argv[]) {

    printf("Input: ");

    char input[MAX] = { 0 } ;

    ////
    // 1. Read user input
    //
    fgets(input, sizeof(input), stdin);

    ////
    // 2. Set the ENTER charater (line-feed) to null-terminated chararacter '\0'
    //
    for (int i = 0; i < (int) sizeof(input); ++i) {
        if (input[i] == 10 || input[i] == 12 || input[i] == 13) { // Line-feed / Page-break / Carriage-return
            input[i] = 0; // terminates string
            break;
        }
    }    
    Print_String(input);

    ////
    // 3. Trim leading and ending space characters
    //
    strtrim(input);
    Print_String(input);

    return 0;
}