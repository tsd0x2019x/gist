/**
 * os_disinguish.c
 * 
 * Demonstrate how to use macro and preprocessor to distinguish operating systems
 *
 */
#if defined(_WIN32) || defined(_WIN64)
void my_function () {
    // CODE HERE
}
#elif defined(__linux__) || defined(__linux) || defined(linux) || \
      defined(__unix__)  || defined(__unix)   || defined(unix) || \
      defined(__APPLE__) || defined(__MACH__)
void my_function () {
    // CODE HERE
}
#else
    #error "OS not supported"
#endif