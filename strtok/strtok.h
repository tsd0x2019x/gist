/**
 * strtok.h
 * 
 * Usage:
 *      Include this header by using the #include directive:
 *              #include "strtok.h"
 * 
 * Compile:
 *      gcc -Wall -Werror -std=gnu99 -pedantic <main_prog.c> -o <main_prog>
 * 
 * where <main_prog> is the name of C-file that imports (includes) the 
 * "strtok.h" header file.
 */
#ifndef STRTOK_H
#define STRTOK_H

#ifdef __cplusplus
extern "C" {
#endif

/*
 * strtok
 * 
 * Splitts (Tokenizes) a given <string> in to many substrings (tokens), that are
 * delimited by characters given in <delimiters>.
 * 
 * @params
 *      <string>:
 *          String that should be splitted (tokenized) in tokens.
 *          This string will remain modified after tokenization.
 *      <delimiters>:
 *          An char array that contains the delimiters for tokenizing
 * 
 * @return
 *      A token (substring) is returned. If <string> is NULL, then
 *      either NULL or the next token will be returned.
 */
char * strtok (char * __restrict__ string, const char * __restrict__ delimiters) {
    if (!delimiters || !(*delimiters)) { // delimiters == NULL || delimiters[0] == '\0'
        return string;
    }
    ////
    // 0. Declare (static) variables
    ////
    int cta = 0; // current token address; distance (offset) from string begin for current token
    static int nta = 0; // next token address; distance (offset) from string begin for next token
    static int ln = 0; // length of <string>
    static char * tok = 0; // pointer to <string>; tok = NULL = 0
    ////
    // 1. If <string> is NULL, return the (next) string token or NULL
    ////
    if ( !string ) { // string == 0 = NULL
        if ( !tok ) {
            return 0;
        }
        cta = nta; // set token address
        if (cta == ln) {
            return 0;
        }
        while (tok[nta]) {
            ++nta;
        }
        while ( !tok[nta] && (nta < ln)) { // (tok[cta] == 0 == '\0') && (nta < ln)
            ++nta;
        }
    }
    else { // string != NULL
        ////
        // 2. Replace all delimiters in <string> by '\0'. Update nta (next token's address)
        ////
        if (!tok) { // if (tok != NULL)
            tok = string;
        }
        int i = 0, j = 0;
        while (string[i]) {
            j = 0;
            while (delimiters[j]) {
                if (string[i] == delimiters[j]) {
                    string[i] = '\0';
                    if (nta == 0) { // Update nta
                        nta = i + 1;
                    }
                    break;
                }
                ++j;
            }
            ++i;
        }
        ln = i; // Set length of <string>
        ////
        // 3. Set valid cta (current token's address) and update nta (next token's address).
        //    Skip (Jump over) all delimiters (replaced by '\0').
        ////
        while ( !tok[cta] ) { // tok[cta] == 0 == '\0'
            cta = nta;
            if (cta == ln) {
                return 0;
            }
            while (tok[nta]) {
                ++nta;
            }
            while ( !tok[nta] && (nta < ln)) { // (tok[cta] == 0 == '\0') && (nta < ln)
                ++nta;
            }
        }
    }
    return (tok + cta);
}

#ifdef __cplusplus
}
#endif

#endif /* STRTOK_H */