/**
 * test.c
 */
#include <stdio.h>
#include <time.h>
#include <string.h>
#include "strtok.h"

const unsigned DURATION = 500000000;

/*
 * Use the built-in libaray function 'strtok'
 */
void test_builtIn_strtok (char * string, const char * delimiters) {
    int i = 1;
    char * tok = strtok(string, delimiters);
    printf("%d. Token: %s\n", i++, tok);
    while ((tok = strtok(NULL, delimiters)) != 0) {
        for (unsigned k=DURATION;k --;);
        printf("%d. Token: %s\n", i++, tok);
    }
}

/*
 * In my own "strtok.h", rename strtok to mstrtok to avoid name conflict.
 * 
 * Use my own implementation 'strtok'
 */
void test_my_strtok (char * string, const char * delimiters) {
    int i = 1;
    char * tok = strtok(string, delimiters); // <-- rename strtok to mstrtok
    printf("%d. Token: %s\n", i++, tok);
    while ((tok = strtok(NULL, delimiters)) != 0) { // <-- rename strtok to mstrtok
        for (unsigned k=DURATION;k--;);
        printf("%d. Token: %s\n", i++, tok);
    }
}

/*
 * Function to measure and compare performance between the built-in strtok and my own strtok
 */
double measure_time (char * string, const char * delimiters, void func(char *, const char *) ) {
    // Time-consuming extra routine to make sure that clock() won't return zero (0)
    //for (unsigned i=0xFFFFFFFF;i--;);
    // Get begin time (time-in-clock)
    clock_t tic = clock();
    // Execute time-consuming function
    func(string, delimiters);
    // Time-consuming extra routine to make sure that clock() won't return zero (0)
    //for (unsigned i=0xFFFFFFFF;i--;);
    // Get end time (time-out-clock)
    clock_t toc = clock();
    // Return time ellapsed (difference)
    return (double) ((toc - tic) / CLOCKS_PER_SEC);
}
int main () {
    char string[]  = "   - This ; is a  ;string; to tokenize.  Right ! ";
    char string2[] = "   - This ; is a  ;string; to tokenize.  Right ! ";
    const char delimiters[] = "; .";
    double time_ellapsed;

    printf("string: %s\n", string);
    printf("delimiters: %s\n", delimiters);

    // My strtok function
    printf("\nRun my own strtok function..\n");
    time_ellapsed = measure_time(string2, delimiters, test_my_strtok);
    printf("Ellapsed time: %.6f (seconds)\n", time_ellapsed);

    // Buit-In strtok function
    printf("\nRun built-in strtok function..\n");
    time_ellapsed = measure_time(string, delimiters, test_builtIn_strtok);
    printf("Ellapsed time: %.6f (seconds)\n", time_ellapsed);

    return 0;
}