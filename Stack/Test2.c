/**
 * Test2.c
 *
 * For testing purposes. Use function "Stack_Create()".
 * 
 * gcc -Wall -Werror -std=gnu99 -pedantic -o Test2 Test2.c
 *
 */
#include <stdio.h>
#include "Stack.h"

int main (int argc, char * argv[]) {

    ////
    // 1. Create an empty stack with default size 100
    //
    Stack * stack = Stack_Create(0);

    ////
    // 2. Push some data chunks on stack.
    //
    Stack_Push(stack, 2);
    Stack_Push(stack, 3);
    Stack_Push(stack, 4); // 4 is now on the top of stack
    printf("Put data 2, 3, 4 on stack.\n");

    ////
    // 3. Get current number of data on stack
    //
    printf("Stack count: %u\n", Stack_Count(stack));

    ////
    // 4. Peek data on the top of stack, without removing it
    //
    int top = Stack_Peek(stack); // similar to Stack_Top(&stack)
    printf("Top data: %d\n", top);

    ////
    // 5. Pop data on the top of stack. That means, remove it from stack
    //
    Stack_Pop(stack);
    printf("Pop top data from stack.\n");

    ////
    // 6. Get current number of data on stack
    //
    printf("Stack count: %u\n", Stack_Count(stack));

    ////
    // 7. Peek data on the top of stack, without removing it
    //
    top = Stack_Top(stack); // similar to Stack_Peek(&stack)
    printf("Top data: %d\n", top);
    
    ////
    // 8. Release stack
    //
    Stack_Release(stack);
}
