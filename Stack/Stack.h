/**
 * Stack.h
 *
 * Implementation of a stack using a (limited) array
 *
 */
#ifndef STACK_H
#define STACK_H

#ifdef __cplusplus
extern "C" {
#endif

//=======================================================================================================================
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define STACK_API extern
#define SIZE 100 // Default stack size
////
// sizeof gives the amount of storage in bytes. For example: sizeof(int) gives 4 bytes for every integer.
// With 4 byes (= 4*8 Bits = 32 Bits) one can represent 2^32 = 4294967296 integer numbers. For the unsigned
// integer numbers, they are from 0 until 4294967295 (= 2^32 -1), i.e. in the range/interval [0, 4294967295].
// For the signed integer numbers, the range [0, 4294967295] is divided into a negative range [-2147483648,-1],
// a postive range [1,2147483647] and one zero (0). They are:
//
// unsigned int:    0, +1, ..., +2147483648, ..., +4294967295
//                  0, +1, ..., +(2^31), ......., +(2^32 -1)
//
// int:     -2147483648, ..., -1, 0, ..., +2147483647
//          -(2^31)    , ..., -1, 0, ..., +(2^31 -1)
//
// For data type <unsigned int>, the expression (1 << 31) evaluates to +(2^31) = +2147483648, and this value
// is valid, since (1 << 31) = +(2^31) = +2147483648 is within the interval [0, 4294967295].
// For data type <int> (signed integer), the expression (1 << 31) evaluates to -(2^31) = -2147483648, since 
// for <int> the greatest value is +(2^31 -1) = +2147483647, the least value is -(2^31) = -2147483648, and 
// (1 << 31) can be evaluated as follows:
//
//      (1 << 31) = +(2^31) = +(2^31) - 1 + 1 = +(2^31 - 1) + 1 = +2147483647 + 1 = (!) -2147483648
//
// Why is the sum of (positive) +2147483647 + 1 = (negative)-2147483648 and NOT (postive) +2147483648 = 2^32.
// Because we only have 32 bits, counted from location 0 until 31. A shift operation (1 << 32) would mean
// that we had 33 bits, counted from 0 until 32. If so, that would cause an INTEGER OVERFLOW to higher range.
// Hence, the addition [+2147483647 + 1] results in an INTEGER OVERFLOW and goes over to the negative range 
// [-2147483648, -1] being left of zero (0). So the next number right of +(2^31 - 1) = +2147483647 is the most-left 
// (least) negative number -2147483648. And there is NO positive number +(2^31) = +2147483648 > +2147483647.
//
// We should AVOID immediately using the expression (1 << 31), since there is simply NO number (1 << 31) = 2^31
// in the range of signed integer.
//
//      (1 << 31) = 2147483648       <==     NO
//      +2147483647                  <==     YES
//      -2147483648                  <==     YES
//
//#define STACK_NONE -pow(2,(8*(int)sizeof(int))-1) // -2^(8*4-1) = -(2^31)
#define STACK_NONE -2147483648 // MINIMUM of <int>

//=======================================================================================================================

typedef struct {
    int * data;
    unsigned int size; // Size of stack
    unsigned int count; // Number of current data in stack
} Stack;

STACK_API void Stack_Init (Stack * stack, unsigned int size);
STACK_API Stack * Stack_Create (unsigned int size);
STACK_API void Stack_Release(Stack * stack);
STACK_API unsigned int Stack_Count (Stack * stack);
STACK_API int Stack_Peek (Stack * stack);
STACK_API int Stack_Top (Stack * stack);
STACK_API int Stack_Pop (Stack * stack);
STACK_API unsigned int Stack_Push (Stack * stack, int value);

/*
 * Stack_Init
 * 
 * Initialize an empty stack. If [size] is 0 (NULL), a stack with default SIZE, e.g. 100,
 * will be initialized. Otherwise, a stack with user-defined size will be initialized.
 */
STACK_API void Stack_Init (Stack * stack, unsigned int size) {
    stack->size = (size == 0) ? SIZE : size;
    stack->data = (int *) malloc(sizeof(int) * stack->size);
    for (int i = 0; i < stack->size; ++i)
        stack->data[i] = STACK_NONE;
    stack->count = 0;
}

/*
 * Stack_Create
 * 
 * Create an (reference of an) empty stack. If [size] is 0 (NULL), a stack with default SIZE,
 * e.g. 100, will be initialized. Otherwise, a stack with user-defined size will be initialized.
 */
STACK_API Stack * Stack_Create (unsigned int size) {
    Stack * stack = (Stack *) malloc(sizeof(Stack));
    Stack_Init(stack, size);
    return stack;
}

/*
 * Stack_Release
 * 
 * Clear the stack. Remove all data. Free allocated memory.
 */
STACK_API void Stack_Release (Stack * stack) {
    if (stack) {
        free(stack->data);
        stack->data = 0;
    }
    stack->size = stack->count = 0;
}

/*
 * Stack_Count
 * 
 * Return the number of data currently being in stack.
 */
STACK_API unsigned int Stack_Count (Stack * stack) {
    return stack->count;
}

/*
 * Stack_Peek
 * 
 * Return the data on the top of the stack without removing it from stack.
 */
STACK_API int Stack_Peek (Stack * stack) {
    int peeked = STACK_NONE;
    if (stack->count == 0) {
        fputs("Warning: function Stack_Peek(): Stack is empty. There's nothing to peek/pop.\n", stdout);
    }
    else { // stack->count > 0
        peeked = stack->data[stack->count-1];
    }
    return peeked;
}

/*
 * Stack_Top
 * 
 * Return the data on the top of the stack without removing it from stack.
 * Similar to Stack_Peek().
 */
STACK_API int Stack_Top (Stack * stack) {
    return Stack_Peek(stack);
}

/*
 * Stack_Pop
 * 
 * Return the data on the top of the stack and remove it from stack.
 */
STACK_API int Stack_Pop (Stack * stack) {
    int peeked = Stack_Peek(stack);
    if (peeked != STACK_NONE) {
        --stack->count;
        stack->data[stack->count] = STACK_NONE;
    }
    return peeked;
}

/*
 * Stack_Push
 * 
 * Put new data (value) on the top of the stack. Return current number of data on stack.
 */
STACK_API unsigned int Stack_Push (Stack * stack, int value) {
    if (stack->count == stack->size)
        fputs("Warning: function Stack_Push(): Stack is full and cannot get more data.\n", stdout);
    else // stack->count < stack->size
        stack->data[stack->count++] = value;
    return stack->count;
}

//=======================================================================================================================

#ifdef __cplusplus
}
#endif

#endif /* STACK_H */