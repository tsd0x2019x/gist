/*
 *
 */
#include <stdio.h>
#include <math.h>

int main(void) {
    double a = pow(2.0, 31);
    int b = (1 << 31); // 4 bytes
    unsigned int c = 1 << 31;
    printf("sizeof(int): %d (bytes)\n", (int)sizeof(int)); // 4 bytes
    printf("a: %f\n", a);
    printf("b: %d\n", b);
    printf("c: %u\n", c);
    return 0;
}