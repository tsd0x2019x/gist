/**
 * StackList.h
 *
 * Implementation of a (unlimited) stack using a linked-list
 *
 */
#ifndef STACKLIST_H
#define STACKLIST_H

#ifdef __cplusplus
extern "C" {
#endif

//=======================================================================================================================
#include <math.h>
#include "LinkedList.h"
#define STACK_API extern
////
// sizeof gives the amount of storage in bytes. For example: sizeof(int) gives 4 bytes for every integer.
// With 4 byes (= 4*8 Bits = 32 Bits) one can represent 2^32 = 4294967296 integer numbers. For the unsigned
// integer numbers, they are from 0 until 4294967295 (= 2^32 -1), i.e. in the range/interval [0, 4294967295].
// For the signed integer numbers, the range [0, 4294967295] is divided into a negative range [-2147483648,-1],
// a postive range [1,2147483647] and one zero (0). They are:
//
// unsigned int:    0, +1, ..., +2147483648, ..., +4294967295
//                  0, +1, ..., +(2^31), ......., +(2^32 -1)
//
// int:     -2147483648, ..., -1, 0, ..., +2147483647
//          -(2^31)    , ..., -1, 0, ..., +(2^31 -1)
//
// For data type <unsigned int>, the expression (1 << 31) evaluates to +(2^31) = +2147483648, and this value
// is valid, since (1 << 31) = +(2^31) = +2147483648 is within the interval [0, 4294967295].
// For data type <int> (signed integer), the expression (1 << 31) evaluates to -(2^31) = -2147483648, since 
// for <int> the greatest value is +(2^31 -1) = +2147483647, the least value is -(2^31) = -2147483648, and 
// (1 << 31) can be evaluated as follows:
//
//      (1 << 31) = +(2^31) = +(2^31) - 1 + 1 = +(2^31 - 1) + 1 = +2147483647 + 1 = (!) -2147483648
//
// Why is the sum of (positive) +2147483647 + 1 = (negative)-2147483648 and NOT (postive) +2147483648 = 2^32.
// Because we only have 32 bits, counted from location 0 until 31. A shift operation (1 << 32) would mean
// that we had 33 bits, counted from 0 until 32. If so, that would cause an INTEGER OVERFLOW to higher range.
// Hence, the addition [+2147483647 + 1] results in an INTEGER OVERFLOW and goes over to the negative range 
// [-2147483648, -1] being left of zero (0). So the next number right of +(2^31 - 1) = +2147483647 is the most-left 
// (least) negative number -2147483648. And there is NO positive number +(2^31) = +2147483648 > +2147483647.
//
// We should AVOID immediately using the expression (1 << 31), since there is simply NO number (1 << 31) = 2^31
// in the range of signed integer.
//
//      (1 << 31) = 2147483648       <==     NO
//      +2147483647                  <==     YES
//      -2147483648                  <==     YES
//
//#define STACK_NONE -pow(2,(8*(int)sizeof(int))-1) // -2^(8*4-1) = -(2^31)
#define STACK_NONE -2147483648 // MINIMUM of <int>

//=======================================================================================================================

typedef struct {
    List * data; // Stack data is represented by a linked-list
} Stack;

STACK_API void Stack_Init (Stack * stack);
STACK_API Stack * Stack_Create ();
STACK_API void Stack_Release(Stack * stack);
STACK_API unsigned int Stack_Count (Stack * stack);
STACK_API int Stack_Peek (Stack * stack);
STACK_API int Stack_Top (Stack * stack);
STACK_API int Stack_Pop (Stack * stack);
STACK_API unsigned int Stack_Push (Stack * stack, int value);

/*
 * Stack_Init
 * 
 * Initialize an empty stack.
 */
STACK_API void Stack_Init (Stack * stack) {
    stack->data = LinkedList_CreateList();
}

/*
 * Stack_Create
 * 
 * Create an empty stack and provide a reference of it.
 */
STACK_API Stack * Stack_Create () {
    Stack * stack = (Stack *) malloc(sizeof(Stack));
    Stack_Init(stack);
    return stack;
}

/*
 * Stack_Release
 * 
 * Clear the stack. Remove all data. Free allocated memory.
 */
STACK_API void Stack_Release (Stack * stack) {
    if (stack) {
        LinkedList_ClearList(stack->data);
        stack->data = 0;
    }
}

/*
 * Stack_Count
 * 
 * Return the number of data currently being in stack.
 */
STACK_API unsigned int Stack_Count (Stack * stack) {
    return stack->data->size;
}

/*
 * Stack_Peek
 * 
 * Return the data on the top of the stack without removing it from stack.
 */
STACK_API int Stack_Peek (Stack * stack) {
    int peeked = STACK_NONE;
    if (Stack_Count(stack) == 0) {
        fputs("Warning: function Stack_Peek(): Stack is empty. There's nothing to peek/pop.\n", stdout);
    }
    else { // Stack_Count(stack) > 0
        peeked = stack->data->end->value;
    }
    return peeked;
}

/*
 * Stack_Top
 * 
 * Return the data on the top of the stack without removing it from stack.
 * Similar to Stack_Peek().
 */
STACK_API int Stack_Top (Stack * stack) {
    return Stack_Peek(stack);
}

/*
 * Stack_Pop
 * 
 * Return the data on the top of the stack and remove it from stack.
 */
STACK_API int Stack_Pop (Stack * stack) {
    int peeked = Stack_Peek(stack);
    if (peeked != STACK_NONE) {
        LinkedList_RemoveLastNode(stack->data);
    }
    return peeked;
}

/*
 * Stack_Push
 * 
 * Put new data (value) on the top of the stack. Return (updated) stack size.
 */
STACK_API unsigned int Stack_Push (Stack * stack, int value) {
    return LinkedList_AddNodeWithValue(stack->data, value); // Add value at list end <=> Push value on top of stack
}

//=======================================================================================================================

#ifdef __cplusplus
}
#endif

#endif /* STACKLIST_H */