#!/bin/bash

SRC="strcmp.c"
DST="test"

if [ -z "$1"]; then
    echo "No c source file given. Using default: $SRC"
else
    SRC=$1
    if [ -z "$2"]; then
        echo "No destination filename given. Using default: $DST"
    else
        DST=$2
    fi
fi

gcc -Wall -Werror -std=gnu99 -pedantic ${SRC} -o ${DST}
echo "Compiled successfully with default source \"$SRC\"" and destination \"$DST\"""
read -n1 -r -p "Press a key to exit.." key
#
# -n1 : expects only one single char as input
# -r  : raw mode
# -p  : activates a prompt
# key : key that is pressed by the user
#