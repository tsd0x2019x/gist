#include <stdio.h>

/*
 * strcmp
 * 
 * Compares two strings str1 and str2. The returned value is one of the followings:
 *      = 0 : str1 and str2 are equal
 *      > 0 : The first unequal char of str1 is greater than of str2
 *      < 0 : The first unequal char of str1 is less than of str2
 * 
 * @params
 *      str1 : First string to compare
 *      str2 : Second string to compare
 *
 * @return
 *      0   : str1 and str2 are equal
 *      > 0 : The first unequal char of str1 is greater than of str2
 *      < 0 : The first unequal char of str1 is less than of str2
 */
int strcmp (char * str1, char * str2) {
    int i = 0;
    while (str1[i] != 0 && str2[i] != 0 && str1[i] == str2[i]) {
        ++i;
    }
    return str1[i] - str2[i];
}

/*
 * remove_newline
 * 
 * Replace the first occurence of the newline char (\n, \r, \r\n) by zero (0, '\0').
 * 
 * @params
 *      buffer
 */
void remove_newline (char * __restrict__ buffer) {
    int i = 0;
    while (buffer[i]) {
        if (buffer[i] == 10 || buffer[i] == 13) {
            buffer[i] = 0; // '\0'
            break;
        }
        ++i;
    }
}

/*
 * Main program for test
 */
int main(int argc, char * argv[]) {
    const int MAX = 100;
    char buffer[MAX];
    char buffer2[MAX];
    int result = 0;

    printf("\n========================\n    Test for strcmp\n========================\n\n");
    printf("First string: ");
    fgets(buffer, MAX, stdin);
    fflush(stdin); // Remaining chars are deleted
    remove_newline(buffer);
    printf("Second string: ");
    fgets(buffer2, MAX, stdin);
    fflush(stdin); // Remaining chars are deleted
    remove_newline(buffer2);
    result = strcmp(buffer,buffer2);

    if (result == 0) {
        printf("Strings %s and %s are equal.\n", buffer, buffer2);
    }
    else if (result < 0) {
        printf("Strings %s and %s are different ( < 0).\n", buffer, buffer2);
    }
    else {
        printf("Strings %s and %s are different ( > 0).\n", buffer, buffer2);
    }
}